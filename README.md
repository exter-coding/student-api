# Student API
### Maintain student records of 5 subjects in local memory for fast request-response fastify is used.
*Getting started*
- clone with SSH
```bash
git clone https://gitlab.com/exter-coding/student-api.git
```
- Install depencies
```bash
npm install
```
- If you want you can install nodemon as dev dpendency (optional)

```bash
npm install -D nodemon
```
- set the PORT and ENV variables in .env file then run using 

```bash
npm start
```

- If using Nodemon run the dev script
```bash
npm run dev
```
*Docs*
- Autogenrated available at (http://localhost:{PORT}/api-docs)
- *Author* :[Promit Revar](https://promit-revar.github.io/Portfolio/)
- *licence* : MIT
