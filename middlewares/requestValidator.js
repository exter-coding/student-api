// Middleware functions to validate request data for add and update requests...
const validate=require('validator');
exports.validateAdd=(data)=>{
    if(!data.hasOwnProperty('studentID')) return false;
    if(!data.hasOwnProperty('studentName')||data['studentName'].length==0|| !validate.isAlpha(data.studentName)) return false;
    if( !data.hasOwnProperty('subject1')|| !Number.isInteger(data.subject1)    || data.subject1<0    || data.subject1>100) return false;
     if(!data.subject2 || !Number.isInteger(data.subject2) || data.subject2<0 || data.subject2>100) return false;
     if(!data.subject4|| !Number.isInteger(data.subject4)|| data.subject4<0|| data.subject4>100) return false;
     if(!data.subject5 || !Number.isInteger(data.subject5) || data.subject5<0 || data.subject5>100) return false;
    if(!data.subject3 || !Number.isInteger(data.subject3) || data.subject3<0 || data.subject3>100) return false;
    return true;
    
}
exports.validateUpdate=(data)=>{
    if(data.hasOwnProperty("studentName") && !validate.isAlpha(data.studentName)) return false;
    if(data.hasOwnProperty("subject1") &&  (!Number.isInteger(data.subject1)    || data.subject1<0    || data.subject1>100)) return false;
    if(data.hasOwnProperty("subject2") &&  (!Number.isInteger(data.subject2)    || data.subject2<0    || data.subject2>100)) return false;
    if(data.hasOwnProperty("subject4") &&  (!Number.isInteger(data.subject4)    || data.subject4<0    || data.subject4>100)) return false;
    if(data.hasOwnProperty("subject5") &&  (!Number.isInteger(data.subject5)    || data.subject5<0    || data.subject5>100)) return false;
    if(data.hasOwnProperty("subject3") &&  (!Number.isInteger(data.subject3)    || data.subject3<0    || data.subject3>100)) return false;
    return true;
}
