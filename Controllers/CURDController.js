const models=require('../storage/database');
const reqValid=require('../middlewares/requestValidator');
exports.landingPage=async (req,reply)=>{                  //Controller function for Landing page...
       
              reply.send({message:"Welcome to student API!!"});
            
}
exports.AddData=(req,reply)=>{                                       // Controller function for adding data...
       if(reqValid.validateAdd(req.body)){
       for(var i=0;i<models.length;i++){
              if(req.body.studentID==models[i].studentID){
                     reply.code(400).send({Error:"bad request",Success:false});
                     return;
              }
       }
       const data={
              studentName:req.body.studentName,
              studentID:req.body.studentID,
              subject1:req.body.subject1,
              subject2:req.body.subject2,
	       subject3:req.body.subject3,
              subject4:req.body.subject4,
	       subject5:req.body.subject5

       }
       models.push(data);
       //console.log(models[models.length-1].studentID+1)
       reply.code(200).send({data:data,Success:true});
}
else{
       reply.code(400).send({Error:"bad request",Success:false});
}

       
       
}  
exports.Report=(req,reply)=>{                                    // Contoller function for getting Report ...
       if(models.length==0){
              reply.code(404).send({error:"No records found!",Success:false})
       }
       else{
       reply.code(200).send({Success:true,data:models});
       }
}
exports.Update=(req,reply)=>{
       if(req.body.hasOwnProperty("studentID") && reqValid.validateUpdate(req.body)){    // Controller function for Updating the record
              var id=req.body.studentID;
              for(var i=0;i<models.length;i++){
                     if(models[i].studentID==id){

                   
              
                     models[i].studentName=req.body.studentName?req.body.studentName:models[i].studentName;
                     models[i].subject1=req.body.subject1?req.body.subject1:models[i].subject1;
                     models[i].subject2=req.body.subject2?req.body.subject2:models[i].subject2;
                     models[i].subject5=req.body.subject5?req.body.subject5:models[i].subject5;
                     models[i].subject3=req.body.subject3?req.body.subject3:models[i].subject3;
                     models[i].subject4=req.body.subject4?req.body.subject4:models[i].subject4;
                     reply.code(200).send( {Success:true,data:models[i]});
                     return;
              }
       }
              
              reply.code(404).send({Error:"No records found",Success:false});
                     
             
              
              
              
       }
       else{
              reply.code(400).send({Error:"bad request",Success:false});
       }
    
}
exports.Delete=(req,reply)=>{
       if(req.body.hasOwnProperty("studentID")){           //Controller function to delete a record...
              var id=req.body.studentID;
              for(var i=0;i<models.length;i++){
                     if(models[i].studentID==id){

                   
                            
                     
                     reply.code(200).send( {Success:true,data:models[i]});
                     models.splice(i,1);
                     return;
              }
       }
              
              reply.code(404).send({Error:"No records found",Success:false});
                     
             
              
              
              
       }
       else{
              reply.code(400).send({Error:"bad request",Success:false});
       }
}
