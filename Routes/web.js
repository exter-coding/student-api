//const addreq=require('../middlewares/requestValidator')

const controllers=require('../Controllers/CURDController');


exports.routes=(fastify,Options,done)=>{
  fastify.register(require('fastify-swagger'), {                     // Documentation plugin to generate auto docs via swagger api
    exposeRoute: true,
    routePrefix: '/api-docs',
    swagger: {
      info: { title: 'Student Api' },
    },
  });
fastify.get('/', controllers.landingPage);      // route for landing page..
fastify.post('/add',controllers.AddData);           // route for adding data page..
fastify.get('/report',controllers.Report);           // route for getting report card page..
fastify.put('/update',controllers.Update);       // route for updating page..
fastify.delete('/delete',controllers.Delete);     // route for deleting data..
   done();
}
