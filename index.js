const dotenv=require('dotenv');
const router=require('./Routes/web');
dotenv.config();
//if dev env then more info about request can be fetched using logger
const fastify = require('fastify')((process.env.ENV=="dev")?{ logger: true }:{logger:false})  
fastify.register(router.routes);

const port=process.env.PORT || 3000;          // set PORT variable in .env or run it over 3000
const start = async () => {
    try {
      await fastify.listen(port,e=>{console.log("Server running on "+port)});
    } catch (err) {
      fastify.log.error(err)
      process.exit(1)
    }
  }
// Function to start the server...
start()
  

